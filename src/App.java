import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.JFileChooser;
public class App {
    public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.print("Ingrese el nombre que utilizará (Ejemplo: S01E): ");
		String str = sc.next();
		System.out.print("Ingrese la extensión deseada (Ejemplo: mkv): ");
		String str1 = sc.next();
		
		JFileChooser chooser = new JFileChooser();
		// chooser.setMultiSelectionEnabled(true);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogTitle("Selección de directorios");
		chooser.setAcceptAllFileFilterUsed(false);

		chooser.showOpenDialog(null);
		File file = chooser.getSelectedFile();

		int[] i = {0};
		try (var path = Files.walk(Paths.get(file.getAbsolutePath()))) {
			path.filter(Files::isRegularFile).forEach(x -> {
				File f = new File(x.toString());
				String name = f.getName();
				name = (name.substring(0, f.getName().lastIndexOf(".")));
				i[0]++;
				if (!str.equals("")) {
					if (i[0] <= 9)
						name = str.concat("0"+i[0]);
					else
						name = str + i[0];
				}
				try {
					Files.move(x, x.resolveSibling(name.concat(".".concat(str1))));
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			
			});
			System.err.println("Ya vo");
		}
		
		sc.close();
	}
}
